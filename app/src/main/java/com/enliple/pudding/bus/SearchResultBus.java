package com.enliple.pudding.bus;

public class SearchResultBus {
    public int type;
    public int count;

    public SearchResultBus(int type, int count) {
        this.type = type;
        this.count = count;
    }
}